=======
History
=======

0.1.0 (Apr 29, 2019)
--------------------

* First release on PyPI.


0.1.1 (Apr 30, 2019)
--------------------

* Documentation patch on PyPI.
