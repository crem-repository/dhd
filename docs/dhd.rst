===
dhd
===

The District Heating Design package is constituted of a series of modules for
constructing the city (*dhd.city*), for connecting the sinks and sources to
the streets network (*dhd.connect*), for seeking the best district heating
network (*dhd.evolve*), for refining and computing the heat loads of the network
(*dhd.load*), for manually modifying the data (*dhd.modify*), for obtaining
district network properties (*dhd.features*) and for visualizing the system
(*dhd.plot*).

.. figure::  images/structure.png
   :align:   center

.. toctree::
   :maxdepth: 1

   city
   connect
   evolve
   features
   load
   modify
   plot
   utils
   exceptions
