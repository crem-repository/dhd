Welcome to DHD's documentation!
*******************************

District Heating Design is a python package developed by the
`CREM <https://www.crem.ch/>`_ for an efficient and systematic design of city
heating networks.

.. figure::  https://gitlab.com/crem-repository/dhd/raw/master/docs/images/example.png
   :align:   center

.. toctree::
   :maxdepth: 1

   overview
   installation
   dhd
   usage
   contributing
   authors
   history

Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
