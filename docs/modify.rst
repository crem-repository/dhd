==========
dhd.modify
==========

Module to search, select and modify specific streets or sinks from the dataframe
*streets* or *sinks*.

The visual *matplotlib* interface of the module *dhd.plot* can be used to
localize geometric object on the map.

Inputs
------

The functions are designed to work on the dataframes *streets* and *sinks*
which can be obtained from the methods *dhd.city.City.get_streets()* and
*dhd.city.City.get_sinks()*.

Functions
---------

.. automodule:: dhd.modify
	:members:
	:private-members:
