# -*- coding: utf-8 -*-

"""Console script for dhd."""
import click


@click.command()
def main():
    """Console script for dhd."""
    return 0


if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
